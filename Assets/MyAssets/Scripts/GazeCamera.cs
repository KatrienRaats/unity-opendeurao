﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GazeCamera : MonoBehaviour
{
    private GameObject xrRig;
    private const float gazeDistance = 10;

    // Start is called before the first frame update
    void Start()
    {
        xrRig = GameObject.FindGameObjectWithTag("Player");
    }

    // Update is called once per frame
    void Update()
    {
        RaycastHit hit;
        if (Physics.Raycast(transform.position, transform.forward, out hit, gazeDistance))
        {
            Debug.Log("Gameobject object hit! " + hit.transform.gameObject.name);
           
        }
       
    }
}
