﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Distance : MonoBehaviour
{
    public Text distanceText;
    private GameObject endPointObject;
    public Transform endPoint;
    private CharacterController myCharacterObject;
    public Transform myCharacter;


    private double distance;

    // Start is called before the first frame update
    void Start()
    {
        myCharacterObject = GetComponent<CharacterController>();
        myCharacter = myCharacterObject.transform;
        endPointObject = GameObject.FindGameObjectWithTag("NextSceneButton");
        endPoint = endPointObject.transform;
    }

    // Update is called once per frame
    void Update()
    {
        distance = System.Math.Round(Vector3.Distance(endPoint.position, myCharacter.position)-4,2);
        distanceText.text = "Distance to goal: " + distance.ToString();
    }
}
