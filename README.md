Asset Store:
School Building
https://assetstore.unity.com/packages/3d/environments/school-assets-146253


Instructions:
Scene 1: TitleScene
Welkomstscherm met naam en roterend logo.
Zal bij volle balk automatisch doorgaan naar Scene 2.

Scene 2: MainScene
Door naar beneden te kijken kan je vooruit bewegen.
Ga binnen in de school om het IT lokaal (IT Room) te zoeken. (2de deur links)
Bij aankijken knoppen komt meer info te voorschijn over de verschillende lagen.

